package com.ithillel.contact_list.model;

import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "personId")
@ToString(exclude = {"numbers"})


public class ContactList {
    private Long personId;
    private String firstName;
    private String lastName;
    private Gender gender;
    private LocalDate birthday;
    private String city;
    private String email;

    private List<PhoneNumber> numbers;


}
