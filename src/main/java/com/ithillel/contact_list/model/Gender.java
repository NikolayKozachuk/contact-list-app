package com.ithillel.contact_list.model;


public enum Gender {
    MALE,
    FEMALE
}
