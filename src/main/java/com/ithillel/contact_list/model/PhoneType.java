package com.ithillel.contact_list.model;

public enum PhoneType {
    MOBILE,
    HOME
}
