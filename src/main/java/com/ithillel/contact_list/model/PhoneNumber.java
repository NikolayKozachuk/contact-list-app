package com.ithillel.contact_list.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "phoneNumberId")
@ToString

public class PhoneNumber {
    private Long phoneNumberId;
    private Long personId;
    private String phone;
    private PhoneType type;

}
