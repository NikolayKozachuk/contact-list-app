package com.ithillel.contact_list.dto;

import com.ithillel.contact_list.model.Gender;
import com.ithillel.contact_list.model.PhoneNumber;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ContactListDto {
    private Long personId;
    private String firstName;
    private String lastName;
    private Gender gender;
    private LocalDate birthday;
    private String city;
    private String email;

}
