package com.ithillel.contact_list.dto.mapper;

import com.ithillel.contact_list.dto.ContactListViewDto;
import com.ithillel.contact_list.model.ContactList;
import org.mapstruct.Mapper;

@Mapper
public interface ContactListViewMapper {
    ContactList fromDto(ContactListViewDto dto);

    ContactListViewDto toDto(ContactList contactList);

}
