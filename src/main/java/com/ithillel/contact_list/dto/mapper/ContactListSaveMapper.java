package com.ithillel.contact_list.dto.mapper;
import com.ithillel.contact_list.dto.ContactListSaveDto;
import com.ithillel.contact_list.model.ContactList;
import org.mapstruct.Mapper;

@Mapper
public interface ContactListSaveMapper {
  ContactList fromDto(ContactListSaveDto dto);
  ContactListSaveDto toDto(ContactList contactList);

}
