package com.ithillel.contact_list.dto.mapper;

import com.ithillel.contact_list.dto.ContactListDto;
import com.ithillel.contact_list.model.ContactList;
import org.mapstruct.Mapper;


    @Mapper
    public interface ContactListDtoMapper {
        ContactList fromDto(ContactListDto dto);
        ContactListDto toDto(ContactList contactList);
    }


