package com.ithillel.contact_list.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ContactListViewDto {
    private Long personId;
    private String firstName;
    private String lastName;
}
