package com.ithillel.contact_list.dao;

import com.ithillel.contact_list.exception.DaoOperationException;
import com.ithillel.contact_list.model.ContactList;
import com.ithillel.contact_list.model.Gender;
import com.ithillel.contact_list.model.PhoneNumber;
import com.ithillel.contact_list.model.PhoneType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ContactListDaoImpl implements ContactListDao {

    private static final Logger LOG = LoggerFactory.getLogger(ContactListDaoImpl.class);

    public static final String FIND_ALL_SQL = "SELECT * FROM persons";
    public static final String FIND_BY_ID_SQL = "SELECT * FROM persons WHERE person_id = ?";

    public static final String DELETE_SQL = "DELETE FROM persons WHERE person_id = ?";
    public static final String CREATE_SQL = "INSERT INTO persons (first_name, last_name, gender, birthday, city, email) VALUES (?, ?, ?, ?, ?, ?);";
    public static final String UPDATE_SQL = "UPDATE persons SET first_name = ?, last_name = ?, gender = ?, birthday = ?, city = ?, email = ? WHERE person_id = ?";


    private DataSource dataSource;

    public ContactListDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<ContactList> findAll() {

      LOG.info("findAll from db in DAO");

        List<ContactList> contactLists = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(FIND_ALL_SQL);

            while (resultSet.next()) {
                ContactList contactList = new ContactList();
                contactList.setPersonId(resultSet.getLong("person_id"));
                contactList.setFirstName(resultSet.getString("first_name"));
                contactList.setLastName(resultSet.getString("last_name"));
                contactList.setGender(Gender.valueOf(resultSet.getString("gender")));
                contactList.setBirthday(resultSet.getDate("birthday").toLocalDate());
                contactList.setCity(resultSet.getString("city"));
                contactList.setEmail(resultSet.getString("email"));

                contactLists.add(contactList);
            }
        } catch (SQLException e) {
            LOG.debug("Error during find all persons");
            throw new DaoOperationException("Error during find all persons", e);
        }
        return contactLists;
    }

    @Override
    public ContactList findById(Long personId) {
        LOG.info("findById from db in DAO");
        Objects.requireNonNull(personId);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_SQL);
            preparedStatement.setLong(1, personId);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                ContactList contactList = new ContactList();
                contactList.setPersonId(resultSet.getLong("person_id"));
                contactList.setFirstName(resultSet.getString("first_name"));
                contactList.setLastName(resultSet.getString("last_name"));
                contactList.setGender(Gender.valueOf(resultSet.getString("gender").toUpperCase()));
                contactList.setBirthday(resultSet.getDate("birthday").toLocalDate());
                contactList.setCity(resultSet.getString("city"));
                contactList.setEmail(resultSet.getString("email"));

                return contactList;
            } else {
                throw new DaoOperationException("Can not find person with person_id " + personId);
            }
        } catch (SQLException e) {
            LOG.debug("Error during find person- {}",personId);
            throw new DaoOperationException("Error during find person", e);
        }
    }

    @Override
    public void remove(Long personId) {
        LOG.info("remove from db in DAO");

        Objects.requireNonNull(personId);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SQL);
            preparedStatement.setLong(1, personId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOG.debug("Error during delete person with id -{} ",personId);
            throw new DaoOperationException("Error during delete person with id = " + personId, e);
        }
    }

    @Override
    public void update(ContactList contactList) {
        LOG.info("update from db in DAO");
        Objects.requireNonNull(contactList);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL);
            preparedStatement.setString(1, contactList.getFirstName());
            preparedStatement.setString(2, contactList.getLastName());
            preparedStatement.setString(3, contactList.getGender().toString());
            preparedStatement.setDate(4, Date.valueOf(contactList.getBirthday()));
            preparedStatement.setString(5, contactList.getCity());
            preparedStatement.setString(6, contactList.getEmail());
            preparedStatement.setLong(7, contactList.getPersonId());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new DaoOperationException("Can not update contact_list with person_id " + contactList.getPersonId());
            }
        } catch (SQLException e) {
            LOG.debug("Error during update contact_list-{}",contactList);
            throw new DaoOperationException("Error during update contact_list", e);
        }
    }

    @Override
    public Long create(ContactList contactList) {
        LOG.info("create from db in DAO");
        Objects.requireNonNull(contactList);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_SQL, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, contactList.getFirstName());
            preparedStatement.setString(2, contactList.getLastName());
            preparedStatement.setString(3, contactList.getGender().toString());
            preparedStatement.setDate(4, Date.valueOf(contactList.getBirthday()));
            preparedStatement.setString(5, contactList.getCity());
            preparedStatement.setString(6, contactList.getEmail());
            preparedStatement.executeUpdate();

            ResultSet generatedKey = preparedStatement.getGeneratedKeys();
            if (generatedKey.next()) {
                long id = generatedKey.getLong("person_id");
                contactList.setPersonId(id);
                return id;
            } else {
                throw new DaoOperationException("No Id returned after create person");
            }
        } catch (SQLException e) {
            LOG.debug("Error create person");
            throw new DaoOperationException("Error create person", e);
        }
    }

}
