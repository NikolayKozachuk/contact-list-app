package com.ithillel.contact_list.dao;

import com.ithillel.contact_list.model.ContactList;

import java.util.List;

public interface ContactListDao {

    List<ContactList> findAll();

    ContactList findById(Long personId);

    void remove(Long personId);

    void update(ContactList contactList);

    Long create(ContactList contactList);


}
