package com.ithillel.contact_list;

import com.ithillel.contact_list.dao.ContactListDao;
import com.ithillel.contact_list.dao.ContactListDaoImpl;
import com.ithillel.contact_list.dto.ContactListDto;
import com.ithillel.contact_list.dto.ContactListSaveDto;
import com.ithillel.contact_list.exception.DaoOperationException;
import com.ithillel.contact_list.model.ContactList;
import com.ithillel.contact_list.model.Gender;
import com.ithillel.contact_list.service.ContactListService;
import com.ithillel.contact_list.service.ContactListServiceImpl;
import com.ithillel.contact_list.util.FileReader;
import com.ithillel.contact_list.util.JdbcUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Runner {

    private static final Logger LOG = LoggerFactory.getLogger(Runner.class);

    private final static String TABLE_INITIALIZATION_SQL_FILE = "db.migration/table_initialization.sql";
    private final static String TABLE_POPULATION_SQL_FILE = "db.migration/table_population.sql";

    private static DataSource dataSource;
    private static ContactListDao contactListDao;
    private static ContactListService contactListService;


    public static void main(String[] args) {
        String db_name = null;

        try {
            db_name = args[0];
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("=== You did not pass the database name, will be used default 'contact_list_db' ===");
        }

        initDatasource(db_name != null ? db_name : "contact_list_db");
        initTablesInDB();
        populateTablesInDB();
        initDao();
        initSvc();

        String allcontacts = "Show all contacts - enter 1";
        String detailContact = "Show details - enter 2";
        String deleteContact = "Delete contact - enter 3";
        String updateContact = "Update contact - enter 4";
        String createContact = "Create new contact - enter 5";
        String whatToDo = "What you want to do? Enter 8 for look possible actions.";
        String end = "Exit - enter 9";
        String menu = allcontacts + "\n" + detailContact + "\n" + deleteContact + "\n" + updateContact + "\n" + createContact + "\n" + end;


        System.out.println(allcontacts);

        Scanner sc = new Scanner(System.in);

        while (!sc.hasNext("9")) {
            String number = sc.nextLine();
            switch (number) {
                case "1":
                    LOG.info("User entered 1");
                    contactListService.findAllPersons().stream()
                            .forEach(System.out::println);
                    System.out.println(detailContact + "\n" + whatToDo + "\n" + end);
                    break;
                case "2":
                    LOG.info("User entered 2");
                    showDetails(sc);
                    System.out.println(whatToDo + "\n" + end);
                    break;
                case "3":
                    LOG.info("User entered 3");
                    removeContact(sc);
                    System.out.println(whatToDo + "\n" + end);

                    break;
                case "4":
                    LOG.info("User entered 4");
                    updateContact(sc);
                    System.out.println(whatToDo + "\n" + end);
                    break;
                case "5":
                    LOG.info("User entered 5");
                    createContact(sc);
                    System.out.println(whatToDo + "\n" + end);
                    break;
                case "8":
                    LOG.info("User entered 8");
                    System.out.println(menu);
                    break;
                default:
                    break;
            }
        }
    }

    private static void showDetails(Scanner sc) {
        System.out.print("Enter ID contact: ");
        String id = sc.nextLine();
        ContactListDto contactList = contactListService.findById(Long.valueOf(id));
        System.out.println(contactList);
    }

    private static void removeContact(Scanner sc) {
        System.out.print("Enter ID contact: ");
        String id = sc.nextLine();
        System.out.println(contactListService.remove(Long.valueOf(id)));

    }

    private static void createContact(Scanner sc) {
        System.out.println("Enter first name: ");
        String firstName = sc.nextLine();
        LOG.info("User entered firstName");
        System.out.println("Enter last name: ");
        String lastName = sc.nextLine();
        LOG.info("User entered lastName");
        System.out.println("Enter gender: MALE or FEMALE");
        String gender = sc.nextLine();
        LOG.info("User entered Gender");
        System.out.println("Enter date of birthday  YYYY:MM:DD (example, 1985-12-05): ");
        String birthdayValue = sc.nextLine();
        LOG.info("User entered dayOfBirthday");
        LocalDate birthday = LocalDate.parse(birthdayValue);
        System.out.println("Enter city: ");
        String city = sc.nextLine();
        LOG.info("User entered city");
        System.out.println("Enter E-mail: ");
        String email = sc.nextLine();
        LOG.info("User entered Email");

        ContactListSaveDto contactListSaveDto = new ContactListSaveDto();
        contactListSaveDto.setFirstName(firstName);
        contactListSaveDto.setLastName(lastName);
        contactListSaveDto.setGender(Gender.valueOf(gender.toUpperCase()));
        contactListSaveDto.setBirthday(birthday);
        contactListSaveDto.setCity(city);
        contactListSaveDto.setEmail(email);

        Long id = contactListService.createNewPerson(contactListSaveDto);
        System.out.println("Create new contact, ID = " + id);

    }

    private static void updateContact(Scanner sc) {
        System.out.print("Enter ID contact for change: ");
        String id = sc.nextLine();
        System.out.println("Here is the selected contact: " + contactListService.findById(Long.valueOf(id)));
        ContactListDto personById = contactListService.findById(Long.valueOf(id));
        System.out.println("Change the first name - enter 1, leave unchanged - enter 2");
        String number = sc.nextLine();
        if (number.equals("1")) {
            System.out.println("Enter first name: ");
            String firstName = sc.nextLine();
            personById.setFirstName(firstName);
        }
        System.out.println("Change the last name - enter 1, leave unchanged - enter 2");
        number = sc.nextLine();
        if (number.equals("1")) {
            System.out.println("Enter last name: ");
            String lastName = sc.nextLine();
            personById.setLastName(lastName);
        }
        System.out.println("Change the gender - enter 1, leave unchanged - enter 2");
        number = sc.nextLine();
        if (number.equals("1")) {
            System.out.println("Enter gender: MALE or FEMALE");
            String gender = sc.nextLine();
            personById.setGender(Gender.valueOf(gender.toUpperCase()));
        }
        System.out.println("Change date of birthday - enter 1, leave unchanged - enter 2");
        number = sc.nextLine();
        if (number.equals("1")) {
            System.out.println("Enter date of birthday  YYYY:MM:DD (example, 1985-12-05): ");
            String birthdayValue = sc.nextLine();
            LocalDate birthday = LocalDate.parse(birthdayValue);
            personById.setBirthday(birthday);
        }
        System.out.println("Change the city - enter 1, leave unchanged - enter 2");
        number = sc.nextLine();
        if (number.equals("1")) {
            System.out.println("Enter city: ");
            String city = sc.nextLine();
            personById.setCity(city);
        }
        System.out.println("Change the  Email - enter 1, leave unchanged - enter 2");
        number = sc.nextLine();
        if (number.equals("1")) {
            System.out.println("Enter E-mail: ");
            String email = sc.nextLine();
            personById.setEmail(email);
        }

        contactListService.updatePerson(personById);
        System.out.println("Contact changed: " + contactListService.findById(Long.valueOf(id)));

    }


    private static void initDatasource(String dbName) {
        dataSource = JdbcUtil.createPostgresDataSource(
                "jdbc:postgresql://localhost:5432/" + dbName, "postgres", "Password1");
    }

    private static void initTablesInDB() {
        String createTablesSql = FileReader.readWholeFileFromResources(TABLE_INITIALIZATION_SQL_FILE);
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.execute(createTablesSql);
            connection.commit();
        } catch (SQLException e) {
            throw new DaoOperationException("Shit happened during tables init.", e);
        }
    }

    private static void populateTablesInDB() {
        String createTablesSql = FileReader.readWholeFileFromResources(TABLE_POPULATION_SQL_FILE);
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.execute(createTablesSql);
            connection.commit();
        } catch (SQLException e) {
            throw new DaoOperationException("Shit happened during tables population.", e);
        }
    }

    private static void initDao() {
        contactListDao = new ContactListDaoImpl(dataSource);
    }

    private static void initSvc() {
        contactListService = new ContactListServiceImpl(dataSource);
    }
}
