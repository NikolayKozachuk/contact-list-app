package com.ithillel.contact_list.service;

import com.ithillel.contact_list.dao.ContactListDao;
import com.ithillel.contact_list.dao.ContactListDaoImpl;
import com.ithillel.contact_list.dto.ContactListDto;
import com.ithillel.contact_list.dto.ContactListSaveDto;
import com.ithillel.contact_list.dto.ContactListViewDto;
import com.ithillel.contact_list.dto.mapper.ContactListDtoMapper;
import com.ithillel.contact_list.exception.DaoOperationException;
import com.ithillel.contact_list.dto.mapper.ContactListSaveMapper;
import com.ithillel.contact_list.dto.mapper.ContactListViewMapper;
import com.ithillel.contact_list.model.ContactList;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

public class ContactListServiceImpl implements ContactListService {

    private static final Logger LOG = LoggerFactory.getLogger(ContactListServiceImpl.class);

    private ContactListDao contactListDao;

    private ContactListDtoMapper contactListDtoMapper = Mappers.getMapper(ContactListDtoMapper.class);
    private ContactListSaveMapper contactListSaveMapper = Mappers.getMapper(ContactListSaveMapper.class);
    private ContactListViewMapper contactListViewMapper = Mappers.getMapper(ContactListViewMapper.class);


    public ContactListServiceImpl(DataSource dataSource) {
        this.contactListDao = new ContactListDaoImpl(dataSource);
    }


    @Override
    public List<ContactListViewDto> findAllPersons() {
        LOG.info("findAllPersons from service");

        List<ContactList> contactLists = contactListDao.findAll();
        List<ContactListViewDto> contactListViewDtos = contactLists.stream().map(p -> contactListViewMapper.toDto(p))
                .collect(Collectors.toList());
        return contactListViewDtos;

    }

    @Override
    public ContactListDto findById(Long id) {
        LOG.info("findById from service");
        ContactList detailContact = contactListDao.findById(id);
        ContactListDto contactListDto = contactListDtoMapper.toDto(detailContact);

        return contactListDto;

    }

    @Override
    public String remove(Long id) {
        LOG.info("remove from service");
        try {
            contactListDao.remove(id);
            contactListDao.findById(id);
            throw new RuntimeException("Should not be here");
        } catch (DaoOperationException e) {
        }
        return "Person with person_id=" + id + " deleted successful";
    }

    @Override
    public Long createNewPerson(ContactListSaveDto dto) {
        LOG.info("CreateNewPerson from service");

        ContactList contactList = contactListSaveMapper.fromDto(dto);

        return contactListDao.create(contactList);

    }

    @Override
    public void updatePerson(ContactListDto dto) {
        LOG.info("updatePerson from service");

        ContactList contactList = contactListDtoMapper.fromDto(dto);

        contactListDao.update(contactList);


    }
}
