package com.ithillel.contact_list.service;

import com.ithillel.contact_list.dto.ContactListDto;
import com.ithillel.contact_list.dto.ContactListSaveDto;
import com.ithillel.contact_list.dto.ContactListViewDto;
import com.ithillel.contact_list.model.ContactList;

import java.util.List;

public interface ContactListService {

   List<ContactListViewDto> findAllPersons();

    ContactListDto findById(Long id);

    String remove(Long id);

    Long createNewPerson(ContactListSaveDto contactList);

    void updatePerson(ContactListDto dto);


}
