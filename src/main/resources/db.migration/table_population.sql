INSERT INTO persons (first_name, last_name, gender, birthday, city, email) VALUES
('Nikolay', 'Kozachuk', 'MALE', '1985-12-09','Kiev', 'chemsplav.cz@gmail.com'),
('Maxim', 'Shevshuk', 'MALE', '1986-11-19','Kiev', 'maxim@gmail.com'),
('Anna', 'Kravets', 'FEMALE', '1995-05-29','Lviv', 'anna@ukr.net'),
('Masha', 'Levchenko', 'FEMALE', '1987-04-07','Dnepr', 'masha@gmail.com'),
('Evgeniy', 'Promuslov', 'MALE', '1986-11-18','Kiev', 'promuslov@gmail.com'),
('Nikolay', 'Kulumnuchenko', 'MALE', '1983-01-10','Rovno', 'kolya@i.ua'),
('Alena', 'Malusheva', 'FEMALE', '1991-12-19','Kiev', 'elena@gmail.com'),
('Dmitriy', 'Zinchenko', 'MALE', '1988-07-12','Odessa', 'dmitriy@yandex.ru'),
('Vladimir', 'Kozachuk', 'MALE', '1990-12-09','Kiev', 'vova@gmail.com'),
('Yulia', 'Bordiuk', 'FEMALE', '1988-03-01','Cherson', 'yulia@ukr.net');

INSERT INTO phone_numbers (person_id, phone, type) VALUES
('1','0937543465', 'MOBILE'),
('2', '0442455576', 'HOME'),
('2', '0672225576', 'MOBILE'),
('3', '0442455576', 'HOME'),
('4', '0632665577', 'MOBILE'),
('5', '0976547898', 'MOBILE'),
('6', '0442455576', 'HOME'),
('7', '0735678790', 'MOBILE'),
('7', '0443214423', 'HOME'),
('10', '0446563312', 'HOME');






