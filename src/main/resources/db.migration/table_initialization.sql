
DROP TABLE IF EXISTS persons,phone_numbers;

CREATE TABLE persons (
  person_id SERIAL,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  gender VARCHAR(10) NOT NULL,
  birthday DATE NOT NULL,
  city VARCHAR(255) NOT NULL,
  email VARCHAR (255) ,
CONSTRAINT persons_PK PRIMARY KEY (person_id),
CONSTRAINT persons_email_AK UNIQUE (email)
);


CREATE TABLE phone_numbers (
  phone_number_id SERIAL ,
  person_id INT NOT NULL,
  phone VARCHAR (255) NOT NULL ,
  type VARCHAR (10) NOT NULL,
  CONSTRAINT phone_number_PK PRIMARY KEY (phone_number_id),
  CONSTRAINT phone_number_persons_FK FOREIGN KEY (person_id) REFERENCES persons (person_id)
  ON DELETE CASCADE
);
